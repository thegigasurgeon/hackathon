import json
import os
from datetime import datetime, timedelta
import json
from flask import Flask, request, jsonify


# Beds, Berm, Drivable Area, Hard Edge, Hedge, Lawn, Mowing 17, Palm Tree, Parking Spot, Pavement, Property, Sidewalk, Soft Edge, Tree
# ft, number, sqft
# area, count, length, perimeter

def aggregate_data(input_folder, output_file):
    print('initiating data aggregation sequence')
    files = os.listdir(input_folder)
    files = [i for i in files if '.json' in i]
    files = [os.path.join(input_folder, i) for i in files]
    
    unified_data = {}
    
    for i in files:
        json_data = json.loads(open(i,'r').read())
        unified_data[json_data['id']] = json_data
        
    
    json.dump(unified_data, open(output_file,'w'), indent=4)    
    print(f'data successfully saved in file "{output_file}"')

# aggregate_data('raw_requests_data', 'data.json')
    
def square_feet_to_acre(square_feet):
    acres = square_feet*(2.29568e-05 )
    return acres


def number_of_features(data, id):
    area_dict = annotated_area(data, id)
    number = len(area_dict.keys())
    return number
    

def timestamp_to_time(timestamp):
    if timestamp == None:
        return -1   
    timestamp = timestamp.split('+')[0]
    timestamp = timestamp.split('.')[0]
    timestamp = timestamp.replace('T', ' ')    
    date = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
    return date   


def status(data, id):
    return data[id]['status']

def eta(data, id):
    if data[id]['eta'] == None:
        return 60.0
    return data[id]['eta']

def initial_eta(data, id):
    return data[id]['initial_eta']

def completed_at(data, id):
    return timestamp_to_time(data[id]['completed_at'])

def generated_at(data, id):
    return timestamp_to_time(data[id]['generated_at'])

def created_at(data, id):
    return timestamp_to_time(data[id]['created_at'])

def address(data, id):
    return data[id]['input']['address']

def parcel_area(data, id):
    return data[id]['input']['parcel_area']


def annotated_area(data, id):
    output = data[id]['outputs']
    
    area_and_perimeter = {}
    
    for i in output:      
        key = i['feature']['name']
        
        area_and_perimeter[key] = {}
        for j in i['measurements']:
            tmp_attribute = j['name']
            tmp_value= j['value']
            area_and_perimeter[key][tmp_attribute] = tmp_value 
            
    return area_and_perimeter


def total_annotated_area(data, id):
    tmp_dict = annotated_area(data, id)
    
    area = 0
    
    for i in list(tmp_dict.keys()):
        if 'area' in tmp_dict[i].keys():
            area = area + tmp_dict[i]['area']
            
    return area

    
def tif_url(data, id):
    return data[id]['tiff_url']

def shareable_link(data, id):
    if len(data[id]['shareable_link']) == 0:
        return 'None'
    
    return data[id]['shareable_link']


def pdf_report_link(data, id):
    pdf_link = data[id]['feature_report']['attributes']['pdf_sample']
    return pdf_link

def excel_report_link(data, id):
    excel_link = data[id]['feature_report']['attributes']['excel_sample']
    return excel_link


def ids_between_date_range(data, start_date, end_date):
    
    ids = list(data.keys())
    required_ids = []
    
    for i in ids:
        created_at_time = created_at(data, i)
        if created_at_time>=start_date and created_at_time<=end_date:
            required_ids.append(i)
    
    return required_ids


def ids_last_30_days(data):
    end_date = datetime.today()
    start_date = end_date - timedelta(30)
    
    required_ids = ids_between_date_range(data, start_date, end_date)
    return required_ids

def ids_last_90_days(data):
    end_date = datetime.today()
    start_date = end_date - timedelta(90)
    
    required_ids = ids_between_date_range(data, start_date, end_date)
    return required_ids


def time_taken(data, id):
    generated = generated_at(data, id)
    completed = completed_at(data, id)
    
    if generated == -1 or completed == -1:
        return eta(data, id)
    
    difference = completed-generated
    return abs(difference.days*24*3600) + abs(difference.seconds)



def calculate_hours_saved(data, id):
    OTC = 0.08
    PSC = 1.50

    no_of_features = number_of_features(data, id)
    area = parcel_area(data, id)
    property_size = square_feet_to_acre(area)

    property_units = round(property_size / 2, 0)

    time_saved_against_tool = property_units * no_of_features * OTC
    time_saved_against_manual_process = property_units * no_of_features * PSC

    if time_saved_against_tool < 0.5:
        tat_saved_against_tool = 0.5
    else:
        tat_saved_against_tool = time_saved_against_tool

    if time_saved_against_manual_process < 6:
        tat_saved_against_manual_process = 6
    else:
        tat_saved_against_manual_process = time_saved_against_manual_process

    return {"against_tool": tat_saved_against_tool, "against_manual_process":tat_saved_against_manual_process}


def get_ids(data):
    return list(set(data.keys()))



def sentences(summary):
    s1 = f'Hey Mark, let us go through the insights we\'ve got for you for #{summary["days"]} days$.'
    s2 = f'You measured #{round(summary["total_parcel_area"])} acres$ of land in this duration.'
    s3 = f'You saved #{round(summary["total_hours_saved_against_manual"])} hours$ on property measurements in this duration, compared to manual estimation process.'
    s4 = f"You measured #{len(summary['total_features'])} types of features$ in this duration, including {', '.join(summary['total_features'][:3])}, etc."    
    s5 = f"You measured #{round(summary['total_classwise_area']['Lawn'])} acres of lawn$ in this duration."
    s6 = f'You measured #{summary["total_unique_sites"]} unique sites$ in this duration.'
    sentence_list = [s1, s2, s3, s4, s5, s6]
    return sentence_list


data = json.loads(open('data.json','r').read())
ids = list(data.keys())

final_json_id_wise = {}
   
for i in ids:
    tmp_dict = {}
    tmp_dict['id'] = i
    tmp_dict['created_at'] = data[i]['created_at']
    tmp_dict['number_of_features'] = number_of_features(data, i)
    tmp_dict['address'] = address(data, i)
    tmp_dict['parcel_area'] = parcel_area(data, i)
    tmp_dict['tif_url'] = tif_url(data, i)
    tmp_dict['classwise_annotated_area'] = annotated_area(data, i)
    tmp_dict['total_annotated_area'] = total_annotated_area(data, i)
    tmp_dict['hours_saved'] = calculate_hours_saved(data, i)
    tmp_dict['time_taken'] = time_taken(data, i)
    final_json_id_wise[i] = tmp_dict


json.dump(final_json_id_wise, open('id_wise.json','w'), indent = 4)

json_date_wise = {}
dates = [data[i]['created_at'] for i in ids]
unique_dates = sorted(set([i.split('T')[0] for i in dates]))

for i in unique_dates:
    tmp_list = []
    
    for j in ids:
        if i in final_json_id_wise[j]['created_at']:
            tmp_list.append(final_json_id_wise[j])
            
    json_date_wise[i] = tmp_list
                
# json.dump(json_date_wise, open('heyo_wise.json','w'), indent = 4)
final_json_date_wise = {}

for i in list(json_date_wise.keys()):
    tmp_parcel_area = 0
    tmp_annotated_area = 0
    tmp_hours_saved_tool = 0
    tmp_hours_saved_manual = 0
    tmp_time_taken = 0
    tmp_features = []
    tmp_count = 0
    tmp_ids = []
    tmp_address= []
    
    for j in json_date_wise[i]: 
        tmp_parcel_area = square_feet_to_acre(tmp_parcel_area + j['parcel_area'])
        tmp_annotated_area = square_feet_to_acre(tmp_annotated_area + j['total_annotated_area'])
        tmp_hours_saved_tool = tmp_hours_saved_tool + j['hours_saved']['against_tool']
        tmp_hours_saved_manual = tmp_hours_saved_manual + j['hours_saved']['against_manual_process']
        tmp_time_taken  = tmp_time_taken + j['time_taken']
        tmp_count = tmp_count + 1
        tmp_ids.append(j['id'])
        keys = list(j['classwise_annotated_area'].keys())
        tmp_features = tmp_features + keys
        tmp_address.append(j['address'])
        
    tmp_features = list(set(tmp_features))
    final_json_date_wise[i] = {"ids":tmp_ids,"total_requests": tmp_count, "features":tmp_features, "parcel_area":tmp_parcel_area, "annotated_area":tmp_annotated_area, "hours_saved_against_tool":tmp_hours_saved_tool, "hours_saved_against_manual":tmp_hours_saved_manual, "time_taken": tmp_time_taken, "addresses": tmp_address}


json.dump(final_json_date_wise, open('date_wise.json','w'), indent = 4)


app = Flask(__name__)

app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True

@app.route('/id_all/')
def idwise():
    return final_json_id_wise


@app.route('/date_all/')
def datewise():
    return final_json_date_wise


@app.route('/id/<request_id>')
def fetch_request_entry(request_id):
    global final_json_id_wise
    #print(final_json_id_wise(request_id))
    return final_json_id_wise[request_id]

@app.route('/date/<date>')
def fetch_date_entry(date):
    global final_json_date_wise
    #print(final_json_id_wise(request_id))
    return final_json_date_wise[date]



@app.route('/date_range/<start_date>/<end_date>')
def fetch_date_range_entry(start_date, end_date):
    try:
        global final_json_date_wise, data
        start_date_s = timestamp_to_time(f'{str(start_date)}T00:00:00')
        end_date_s = timestamp_to_time(f'{str(end_date)}T23:59:59')
    
        required_ids = ids_between_date_range(data, start_date_s, end_date_s)
        
        
        tmp_required_entries = []
        
        for i in required_ids:
            tmp_required_entries.append(final_json_id_wise[i])
            #for j in list(final_json_date_wise.keys()):
                #if i in final_json_date_wise[j]['ids']:
                    #tmp_required_entries.append(final_json_date_wise[j])
                    
        
        
        required_entries = []
        
        
        for i in tmp_required_entries:
            if i not in required_entries:
                required_entries.append(i)
        
        
        total_requests, total_parcel_area, total_annotated_area, total_hours_saved_against_tool, total_hours_saved_against_manual, total_time_taken = 0, 0, 0, 0, 0, 0  
        total_features = []
        total_classwise_area = {}
        total_addresses = []
        
        for i in required_entries:
            total_annotated_area = total_annotated_area + i['total_annotated_area']
            total_hours_saved_against_manual = total_hours_saved_against_manual + i['hours_saved']['against_manual_process']
            total_hours_saved_against_tool = total_hours_saved_against_tool + i['hours_saved']['against_tool']
            total_parcel_area = total_parcel_area + i['parcel_area']
            total_time_taken = total_time_taken + i['time_taken']
            total_addresses.append(i['address'])
            total_features = total_features + list(i['classwise_annotated_area'].keys())
            
            for j in list(i['classwise_annotated_area'].keys()):
                if j not in total_classwise_area.keys():
                    if 'area' in i['classwise_annotated_area'][j].keys():
                        total_classwise_area[j] = square_feet_to_acre(i['classwise_annotated_area'][j]['area'])
                    
                else:
                    # print(type(total_classwise_area), type(i['classwise_annotated_area'][j]))
                    total_classwise_area[j] = total_classwise_area[j] + square_feet_to_acre(i['classwise_annotated_area'][j]['area'])
            
            
        summary = {}
        summary['days'] = (end_date_s - start_date_s).days + 1
        summary['total_annotated_area'] = square_feet_to_acre(total_annotated_area)
        summary['total_hours_saved_against_manual'] = total_hours_saved_against_manual
        summary['total_features'] = list(set(total_features))
        summary['total_hours_saved_against_tool'] = total_hours_saved_against_tool
        summary['total_parcel_area'] = square_feet_to_acre(total_parcel_area)
        summary['total_time_taken'] = total_time_taken
        summary['total_requests'] = len(required_entries)
        summary['total_classwise_area'] = total_classwise_area
        summary['total_unique_sites'] = len(set(total_addresses))
        
        sentences_list = sentences(summary)
        
    
        datewise_entries =  {}
        
        for i in required_ids:
            for j in list(final_json_date_wise.keys()):
                if i in final_json_date_wise[j]['ids']:
                    datewise_entries[j] = final_json_date_wise[j]
        
    
        return {'summary':summary, 'raw_entries':required_entries, 'datewise_entries':datewise_entries ,'sentences':sentences_list}
    
    except Exception as e:
        return jsonify(str(e))

    
app.run()


    
